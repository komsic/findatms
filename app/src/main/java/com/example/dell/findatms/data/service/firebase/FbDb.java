package com.example.dell.findatms.data.service.firebase;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.dell.findatms.data.db.Check;
import com.example.dell.findatms.data.db.User;
import com.example.dell.findatms.data.db.Review;
import com.example.dell.findatms.data.service.firebase.model.FbAtm;
import com.example.dell.findatms.data.service.firebase.model.FbUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FbDb {

    private static final String TAG = "FbDb";

    private static FbDb INSTANCE;
    private DatabaseReference mAtmDbRef;
    private DatabaseReference mUserDbRef;
    private DatabaseReference mCheckDbRef;

    private FbDb(String uid) {
        DatabaseReference rootDbRef = FirebaseDatabase.getInstance().getReference();
        mAtmDbRef = rootDbRef.child("atm");
        mUserDbRef = rootDbRef.child("users").child(uid);
        mCheckDbRef = mUserDbRef.child("check");
    }

    public static FbDb getInstance(String uid) {
        if (INSTANCE == null) {
            INSTANCE = new FbDb(uid);
        }

        return INSTANCE;
    }

    public void getFbAtm(String id, ValueEventListener reviewList) {
        mAtmDbRef.child(id).addListenerForSingleValueEvent(reviewList);
    }

    public void getFbUser(ValueEventListener userEventListener) {
        mUserDbRef.addListenerForSingleValueEvent(userEventListener);
    }

    public void addReviewToFb(Review review, String userId, String atmId) {
        Map<String, Review> reviewMap = new HashMap<>();
        reviewMap.put(userId, review);
        FbAtm atm = new FbAtm(reviewMap);

        Map<String, Object> reviewValues = atm.toMap();

        Map<String, Object> childUpdate = new HashMap<>();
        childUpdate.put(atmId, reviewValues);

        mAtmDbRef.child(atmId).updateChildren(childUpdate, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable final DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                if (databaseError == null) {
                    databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            String key = dataSnapshot.getKey();
                            String ref = dataSnapshot.getRef().toString();
                            FbAtm atm1 = dataSnapshot.getValue(FbAtm.class);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

//        mAtmDbRef.updateChildren(childUpdate, new DatabaseReference.CompletionListener() {
//            @Override
//            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
//                if (databaseError != null) {
//                    Log.e(TAG, "onComplete: " + databaseError.getMessage());
//                }
//                Log.e(TAG, "onComplete: " + databaseReference);
//
//                databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        FbAtm fbAtm = dataSnapshot.getValue(FbAtm.class);
//                        Log.e(TAG, "onDataChange: " + dataSnapshot.getKey());
//                        if (fbAtm != null) {
//                            Map<String, Review> reviewMapList = fbAtm.reviews;
//                            if (reviewMapList != null) {
//                                Log.e(TAG, "onDataChange: " + dataSnapshot.getRef());
//                            } else {
//                                Log.e(TAG, "onDataChange: reviewMap is null");
//                            }
//                        } else {
//                            Log.e(TAG, "onDataChange: mNearByAtmDao.updateReviewList() not updated");
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });
//            }
//        });
    }

    public void addCheckInToFb(Check check, String atmId) {
        mCheckDbRef.child(atmId).setValue(check);
    }

    public void saveUserToFb(User user) {
        FbUser fbUser = new FbUser();
        fbUser.checks = new ArrayList<>();
        fbUser.userInfo = user;
        mUserDbRef.setValue(fbUser);
    }

    public interface ReviewListRequestResult {
        void onReviewListReturned(ArrayList<Review> reviews, String id);
    }
}
//    Review review = new Review();
//        review.setRating(4.0);
//                review.setMessage("Open ATM for withdrawals. Really safe");
//                review.setUserId("komsic");
//
//                Review review1 = new Review();
//                review1.setRating(2.0);
//                review1.setMessage("Open ATM for withdrawals. Really safe");
//                review1.setUserId("abbey");
//
//                Review review2 = new Review();
//                review2.setRating(5.0);
//                review2.setMessage("Open ATM for withdrawals. Really safe");
//                review2.setUserId("nikky");
//
//                Review review3 = new Review();
//                review3.setRating(2.0);
//                review3.setMessage("Open ATM for withdrawals. Really safe");
//                review3.setUserId("johnny");
//
//                ArrayList<Review> reviews = new ArrayList<>();
//        reviews.add(review);
//        reviews.add(review1);
//        reviews.add(review2);
//        reviews.add(review3);
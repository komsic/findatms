package com.example.dell.findatms.data.service.firebase.model;

import com.example.dell.findatms.data.db.Check;
import com.example.dell.findatms.data.db.User;

import java.util.List;

public class FbUser {

    public User userInfo;
    public List<Check> checks;
}

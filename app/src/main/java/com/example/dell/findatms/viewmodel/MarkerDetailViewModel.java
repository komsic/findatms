package com.example.dell.findatms.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.dell.findatms.data.AppRepository;
import com.example.dell.findatms.data.db.NearByAtm;

public class MarkerDetailViewModel extends ViewModel {

    private AppRepository mRepository;

    MarkerDetailViewModel(AppRepository repository) {
        mRepository = repository;
    }

    public LiveData<NearByAtm> getNearByAtm(String id) {
        return mRepository.getNearByAtmById(id);
    }
}

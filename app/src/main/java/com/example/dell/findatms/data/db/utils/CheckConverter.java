package com.example.dell.findatms.data.db.utils;

import android.arch.persistence.room.TypeConverter;

import com.example.dell.findatms.data.db.CheckTime;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class CheckConverter {

    @TypeConverter
    public static List<CheckTime> fromString(String value) {
        Type listType = new TypeToken<List<CheckTime>>() {
        }.getType();

        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromList(List<CheckTime> checkTimes) {
        return new Gson().toJson(checkTimes);
    }
}

package com.example.dell.findatms.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.example.dell.findatms.FindAtmApp;
import com.example.dell.findatms.data.AppRepository;

public class ViewModelFactory implements ViewModelProvider.Factory {

    private final AppRepository mRepository;

    public ViewModelFactory() {
        mRepository = FindAtmApp.getRepository();
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(MainViewModel.class)) {
            return (T) new MainViewModel(mRepository);
        } else if (modelClass.isAssignableFrom(MarkerDetailViewModel.class)) {
            return (T) new MarkerDetailViewModel(mRepository);
        } else if (modelClass.isAssignableFrom(SignInViewModel.class)) {
            return (T) new SignInViewModel(mRepository);
        } else if (modelClass.isAssignableFrom(NewUserViewModel.class)) {
            return (T) new NewUserViewModel(mRepository);
        } else if (modelClass.isAssignableFrom(ReviewViewModel.class)) {
            return (T) new ReviewViewModel(mRepository);
        } else {
            throw new IllegalArgumentException("ViewModel Not Found");
        }
    }
}

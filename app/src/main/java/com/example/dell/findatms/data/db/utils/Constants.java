package com.example.dell.findatms.data.db.utils;

public class Constants {

    public static final String KEY_PREF_LAST_GEOFENCE_TRANSITION = "PREF_LAST_GEOFENCE_TRANSITION";
    public static final String KEY_PREF_TIME_IN = "PREF_TIME_IN";
}

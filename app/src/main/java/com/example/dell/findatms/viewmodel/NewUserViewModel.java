package com.example.dell.findatms.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.example.dell.findatms.data.AppRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;

public class NewUserViewModel extends ViewModel {

    private AppRepository mRepository;

    public NewUserViewModel(AppRepository repository) {
        mRepository = repository;
    }

    public void createUserWithEmailAndPassword(String email, String password, String fullName,
                                               String userName,
                                               OnCompleteListener<AuthResult> completeListener) {
        mRepository.createUserWithEmailAndPassword(
                email.trim(),
                password,
                fullName.trim(),
                userName.trim(),
                completeListener);
    }
}

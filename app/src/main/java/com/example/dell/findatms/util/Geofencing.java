package com.example.dell.findatms.util;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.dell.findatms.data.db.NearByAtm;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class Geofencing {
    public static final String KEY_EXTRA_ATM_NAME = "atm name";
    public static final String KEY_EXTRA_ATM_ID = "atm id";
    private static final String TAG = "Geofencing";
    private static final long GEOFENCE_TIMEOUT = 3600000;
    private static final float GEOFENCE_RADIUS = 50;
    private Geofence mGeofence;
    private PendingIntent mGeofencePendingIntent;
    private Context mContext;

    public Geofencing(Context context) {
        mContext = context;
        mGeofencePendingIntent = null;
        mGeofence = null;
    }

    public void registerGeofence(String atmName, String atmId) {
        try {
            LocationServices.getGeofencingClient(mContext).addGeofences(
                    getGeofencingRequest(),
                    getGeofencePendingIntent(atmName, atmId)
            ).addOnSuccessListener(((Activity) mContext), new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.e(TAG, "geofence registered");
                }
            }).addOnFailureListener(((Activity) mContext), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "failed to registered geofence" + "\n" + e.getMessage());
                }
            });
        } catch (SecurityException securityException) {
            Log.e(TAG, securityException.getMessage());
        }
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofence(mGeofence);

        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent(String atmName, String atmId) {
        Intent intent = new Intent(mContext, GeofenceIntentService.class);
        intent.putExtra(KEY_EXTRA_ATM_NAME, atmName);
        intent.putExtra(KEY_EXTRA_ATM_ID, atmId);
        mGeofencePendingIntent = PendingIntent.getService(mContext, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        return mGeofencePendingIntent;
    }

    public void unRegisterGeofence() {
        try {
            LocationServices.getGeofencingClient(mContext).removeGeofences(getGeofencePendingIntent(null, null))
                    .addOnSuccessListener(((Activity) mContext), new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.e(TAG, "geofence registered");
                        }
                    }).addOnFailureListener(((Activity) mContext), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "failed to remove geofence" + "\n" + e.getMessage());
                }
            });
        } catch (SecurityException securityException) {
            Log.e(TAG, securityException.getMessage());
        }
    }

    public void updateGeofence(NearByAtm atm) {
        if (atm == null) {
            return;
        }

        String atmId = atm.getId();
        double atmLat = atm.getLat();
        double atmLng = atm.getLng();

        mGeofence = new Geofence.Builder()
                .setRequestId(atmId)
                .setExpirationDuration(GEOFENCE_TIMEOUT)
                .setCircularRegion(atmLat, atmLng, GEOFENCE_RADIUS)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER
                        | Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
    }
}

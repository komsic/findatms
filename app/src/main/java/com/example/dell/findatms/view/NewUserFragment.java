package com.example.dell.findatms.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.findatms.R;
import com.example.dell.findatms.viewmodel.NewUserViewModel;
import com.example.dell.findatms.viewmodel.ViewModelFactory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

import static com.example.dell.findatms.view.MainActivity.KEY_EXTRA_IS_SIGN_IN;

public class NewUserFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "NewUserFragment";

    private TextInputEditText passwordEditText;
    private TextInputEditText emailEditText;
    private TextInputEditText fullNameEditText;
    private TextInputEditText usernameEditText;

    private TextInputLayout emailInputLayout;
    private TextInputLayout fullNameInputLayout;
    private TextInputLayout passwordInputLayout;
    private TextInputLayout usernameInputLayout;
    private ProgressBar mProgressBar;

    private NewUserViewModel mViewModel;

    public NewUserFragment() {
        // Required empty public constructor
    }

    public static NewUserFragment newInstance() {
        NewUserFragment fragment = new NewUserFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_user, container, false);

        passwordEditText = rootView.findViewById(R.id.password_text_edit);
        emailEditText = rootView.findViewById(R.id.email_text_edit);
        fullNameEditText = rootView.findViewById(R.id.full_name_text_edit);
        usernameEditText = rootView.findViewById(R.id.user_text_edit);

        emailInputLayout = rootView.findViewById(R.id.email_text_input);
        fullNameInputLayout = rootView.findViewById(R.id.full_name_text_input);
        usernameInputLayout = rootView.findViewById(R.id.user_text_input);
        passwordInputLayout = rootView.findViewById(R.id.password_text_input);

        mProgressBar = rootView.findViewById(R.id.progress_bar);

        TextView signUpText = rootView.findViewById(R.id.text_sign_up);
        signUpText.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ViewModelFactory viewModelFactory = new ViewModelFactory();
        mViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(NewUserViewModel.class);
    }

    @Override
    public void onDestroyView() {
        mProgressBar.setVisibility(View.GONE);
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.text_sign_up) {
            String email = emailEditText.getText().toString().trim();
            String password = passwordEditText.getText().toString();
            String fullName = fullNameEditText.getText().toString().trim();
            String userName = usernameEditText.getText().toString().trim();

            if (email.contains("@") && email.length() > 4 &&
                    password.length() >= 8 &&
                    !fullName.isEmpty() &&
                    !userName.isEmpty()) {
                usernameInputLayout.setError(null);
                emailInputLayout.setError(null);
                passwordInputLayout.setError(null);
                fullNameInputLayout.setError(null);

                OnCompleteListener<AuthResult> completeListener = new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            openMainActivity();
                        } else {
                            issueError();
                        }
                    }
                };

                mViewModel.createUserWithEmailAndPassword(
                        email,
                        password,
                        fullName,
                        userName,
                        completeListener);
                mProgressBar.setVisibility(View.VISIBLE);
            } else {
                if (!email.contains("@") && email.length() < 4) {
                    emailInputLayout.setError("Email is not valid");
                } else {
                    emailInputLayout.setError(null);
                }
                if (password.length() < 8) {
                    passwordInputLayout.setError("Password must be more than 8 characters");
                } else {
                    passwordInputLayout.setError(null);
                }
                if (fullName.isEmpty()) {
                    fullNameInputLayout.setError("Full Name can not be empty");
                } else {
                    fullNameInputLayout.setError(null);
                }
                if (userName.isEmpty()) {
                    usernameInputLayout.setError("Username can not be empty");
                } else {
                    usernameInputLayout.setError(null);
                }
            }
        }
    }

    public void openMainActivity() {
        Intent intent = MainActivity.getStartIntent(getActivity());
        intent.putExtra(KEY_EXTRA_IS_SIGN_IN, true);
        startActivity(intent);
        mProgressBar.setVisibility(View.GONE);
        getActivity().finish();
    }

    public void issueError() {
        mProgressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), "Error creating user. Please retry",
                Toast.LENGTH_SHORT).show();
    }
}

package com.example.dell.findatms.data.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

@Dao
public interface UserDao {

    @Query("SELECT * FROM User WHERE id = :id")
    User getCurrentUser(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long setUser(User user);

    @Query("DELETE FROM User")
    int delete();
}

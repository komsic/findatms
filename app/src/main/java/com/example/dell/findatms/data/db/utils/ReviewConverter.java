package com.example.dell.findatms.data.db.utils;

import android.arch.persistence.room.TypeConverter;

import com.example.dell.findatms.data.db.Review;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ReviewConverter {

    @TypeConverter
    public static ArrayList<Review> fromString(String value) {
        Type listType = new TypeToken<ArrayList<Review>>() {
        }.getType();

        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(ArrayList<Review> reviews) {
        Gson gson = new Gson();

        return gson.toJson(reviews);
    }
}

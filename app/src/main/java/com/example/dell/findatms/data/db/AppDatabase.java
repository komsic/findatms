package com.example.dell.findatms.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {User.class, Check.class, NearByAtm.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getInMemoryDatabase(Context context) {

        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, AppDatabase.class, "find_atm_database")
                    .allowMainThreadQueries()
                    .build();
        }

        return INSTANCE;
    }

    public abstract UserDao userModel();

    public abstract CheckDao checkModel();

    public abstract NearByAtmDao nearByAtmModel();
}

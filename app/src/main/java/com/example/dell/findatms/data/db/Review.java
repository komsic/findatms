package com.example.dell.findatms.data.db;

import android.arch.persistence.room.Ignore;

public class Review {

    private String userId;
    private String message;
    private Double rating;

    public Review() {
    }

    @Ignore
    public Review(String user, float numStars, String text) {
        userId = user;
        message = text;
        rating = (double) numStars;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Double getRating() {
        if (rating == null) {
            return 0.0;
        }
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

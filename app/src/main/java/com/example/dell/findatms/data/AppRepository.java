package com.example.dell.findatms.data;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.dell.findatms.data.db.AppDatabase;
import com.example.dell.findatms.data.db.Check;
import com.example.dell.findatms.data.db.CheckDao;
import com.example.dell.findatms.data.db.CheckTime;
import com.example.dell.findatms.data.db.NearByAtm;
import com.example.dell.findatms.data.db.NearByAtmDao;
import com.example.dell.findatms.data.db.User;
import com.example.dell.findatms.data.db.UserDao;
import com.example.dell.findatms.data.service.network.Remote;
import com.example.dell.findatms.data.service.firebase.FbAuth;
import com.example.dell.findatms.data.service.firebase.FbDb;
import com.example.dell.findatms.data.service.firebase.model.FbAtm;
import com.example.dell.findatms.data.service.firebase.model.FbUser;
import com.example.dell.findatms.data.service.network.place.Result;
import com.example.dell.findatms.data.db.Review;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppRepository implements FirebaseAuth.AuthStateListener {

    private static final String TAG = "AppRepository";
    private static AppRepository INSTANCE;

    private UserDao mUserDao;
    private CheckDao mCheckDao;
    private NearByAtmDao mNearByAtmDao;

    private LiveData<List<Check>> mAllCheckIns;
    private LiveData<List<NearByAtm>> mNearByAtms;

    private Remote mRemote;
    private FbDb mFbDb;
    private FbAuth mFbAuth;

    private DisplayError mDisplayError;
    private LatLng mUserLocation;

    private AppRepository(Context context) {
        AppDatabase database = AppDatabase.getInMemoryDatabase(context);

        mUserDao = database.userModel();

        mCheckDao = database.checkModel();
        mAllCheckIns = mCheckDao.findAllCheckIns();

        mNearByAtmDao = database.nearByAtmModel();
        mNearByAtms = mNearByAtmDao.getAllNearByAtms();

        mRemote = new Remote();
        mFbAuth = FbAuth.getInstance();
    }

    public void initializeUser(boolean isFirstSignIn) {
        User user = mFbAuth.getUser();
        if (user != null) {
            mFbAuth.addAuthStateListener(this);
            mFbDb = FbDb.getInstance(user.getId());
            if (isFirstSignIn) {
                mFbDb.saveUserToFb(user);
            }

            ValueEventListener userEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    FbUser fbUser = dataSnapshot.getValue(FbUser.class);
                    Log.e(TAG, "onDataChange: " + dataSnapshot.getRef());
                    if (fbUser != null) {
                        if (fbUser.userInfo != null) {
                            setUser(fbUser.userInfo);
                            Log.e(TAG, "onDataChange: " + fbUser.userInfo.getId());
                        } else {
                            Log.e(TAG, "onDataChange: " + "setUser failed ");
                        }

                        List<Check> checkList = fbUser.checks;
                        if (checkList != null && checkList.size() > 0) {
                            for (Check check : checkList) {
                                insertCheckIn(check);
                            }
                        } else {
                            Log.e(TAG, "onDataChange: " + "checkList is null");
                        }
                    } else {
                        Log.e(TAG, "onDataChange: " + "fbUser is null");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };

            mFbDb.getFbUser(userEventListener);
        }
    }

    public void setUser(User user) {
        long i = mUserDao.setUser(user);
        Log.e(TAG, "setUser: " + i);
    }

    private void insertCheckIn(Check check) {
        long i = mCheckDao.insertCheckIn(check);
        Log.e(TAG, "insertCheckIn: " + i);
    }

    public static AppRepository getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new AppRepository(context);
        }
        return INSTANCE;
    }

    private void initializePlace(List<Result> results) {
        deleteAllNearByAtms();
        if (results != null && results.size() > 0) {
            for (Result result : results) {

                NearByAtm atm = new NearByAtm(result.getId(), result.getName(),
                        result.getGeometry().getLocation().getLat(),
                        result.getGeometry().getLocation().getLng(),
                        getCheckInForAtm(result.getId()));

                //mRemote.getDistance().getRows()
                //                                .get(0).getElements().get(results.indexOf(result))
                //                                .getDistance().getText(),
                //                        mRemote.getDistance().getRows()
                //                                .get(0).getElements().get(results.indexOf(result))
                //                                .getDuration().getText()

                long i = insertNearByAtm(atm);
                Log.e(TAG, "initializePlace: insertNearByAtm() " + i);

                mFbDb.getFbAtm(result.getId(), new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        FbAtm fbAtm = dataSnapshot.getValue(FbAtm.class);
                        Log.e(TAG, "onDataChange: " + dataSnapshot.getKey());
                        if (fbAtm != null) {
                            if (fbAtm.reviews != null) {
                                Map<String, Review> reviewMap = fbAtm.reviews;
                                ArrayList<Review> reviewArrayList = new ArrayList<>();
                                for (String key : reviewMap.keySet()) {
                                    Review review = reviewMap.get(key);
                                    reviewArrayList.add(review);
                                }
                                mNearByAtmDao.updateReviewList(reviewArrayList,
                                        dataSnapshot.getKey());
                                Log.e(TAG, "onDataChange: " + dataSnapshot.getRef());
                            } else {
                                Log.e(TAG, "onDataChange: fbAtm review is null");
                            }
                        } else {
                            Log.e(TAG, "onDataChange: mNearByAtmDao.updateReviewList() not updated");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG, "onCancelled: " + databaseError.getMessage());
                        Log.e(TAG, "onCancelled: " + databaseError.getDetails());
                    }
                });
            }
        }
    }

    private void updateAtmDistanceAndTime(String id, String dist, String time) {
        int i = mNearByAtmDao.updateDistanceAndTime(id, dist, time);
        Log.e(TAG, "updateAtmDistanceAndTime: " + i);
    }

    private int getCheckInForAtm(String atmId) {
        int check = 0;
        Check checkIn = mCheckDao.findCheckInByAtmId(atmId);
        if (checkIn != null) {
            check = checkIn.getChecks().size();
        }

        return check;
    }

    private void deleteAllNearByAtms() {
        int i = mNearByAtmDao.deleteAll();
        Log.e(TAG, "deleteAllNearByAtms: " + i);
    }

    private long insertNearByAtm(NearByAtm atm) {
        return mNearByAtmDao.insertNearByAtm(atm);
    }

    private void getDirectionById(NearByAtm atm) {
        if (mRemote.getDirection() != null) {
            atm.setPath(mRemote.getDirection().getRoutes().get(0).getOverviewPolyline().getPoints());
        }
    }

    public LiveData<List<Check>> getAllCheckIns() {
        return mAllCheckIns;
    }

    public Check getCheckInForAParticularAtm(String atmId) {
        return mCheckDao.findCheckInByAtmId(atmId);
    }

    public User getCurrentUser(String id) {
        return mUserDao.getCurrentUser(id);
    }

    public LiveData<List<NearByAtm>> getNearByAtms() {
        return mNearByAtms;
    }

    public LiveData<NearByAtm> getNearByAtmById(String id) {
        return mNearByAtmDao.getNearByAtmById(id);
    }

    public LiveData<String> getPathToAtm(String id) {
        return mNearByAtmDao.getPathToAtm(id);
    }

    public void searchNearByAtms(final double latitude, final double longitude) {
        mRemote.searchNearByAtms(latitude, longitude, new Remote.NearByResult() {
            @Override
            public void onSuccessful(List<Result> results) {
                mRemote.getDistance(results, latitude, longitude, new Remote.DistanceApiResult() {
                    @Override
                    public void onSuccessful(String id, String dist, String time) {
                        updateAtmDistanceAndTime(id, dist, time);
                    }

                    @Override
                    public void onFailed(String error) {
                        mDisplayError.issueError(error);
                    }
                });
                initializePlace(results);
            }

            @Override
            public void onFailed(String error) {
                mDisplayError.issueError(error);
            }
        });
    }

    public void getPathToAtmFromService(NearByAtm atm) {
        mRemote.getPathToAtmFromService(atm, mUserLocation.latitude,
                mUserLocation.longitude, new Remote.DirectionToNearByAtmResult() {
            @Override
            public void onSuccessful(NearByAtm atm) {
                int i = mNearByAtmDao.setPathToAtm(atm.getId(), atm.getPath());
                Log.e(TAG, "onSuccessful: " + i);
            }

            @Override
            public void onFailed(String error) {
                mDisplayError.issueError(error);
            }
        });
    }

    public void addUserReview(String text, float rating, String atmId) {
        Review review = new Review("ola", rating, text);
        NearByAtm atm = mNearByAtmDao.getNearByAtmById(atmId).getValue();
        if (atm != null) {
            ArrayList<Review> reviews = atm.getReviews();
            reviews.add(review);

            mNearByAtmDao.updateReviewList(reviews, atmId);
        }
        mFbDb.addReviewToFb(review, mFbAuth.getUid(), atmId);
    }

    public void addCheckIn(CheckTime checkTime, String atmId, String atmName) {
        Check check = mCheckDao.findCheckInByAtmId(atmId);
        if (check == null) {
            check = new Check(atmId, atmName);
        }

        check.addCheckIn(checkTime);
        int i = mCheckDao.updateCheckInList(check.getChecks(), atmId);
        Log.e(TAG, "addCheckIn: " + i);
        mFbDb.addCheckInToFb(check, atmId);
    }

    public void signIn(String email, String password,
                       OnCompleteListener<AuthResult> completeListener) {
        mFbAuth.signIn(email, password, completeListener);
    }

    public void createUserWithEmailAndPassword(String email, String password, String fullName,
                                               String userName,
                                               OnCompleteListener<AuthResult> completeListener) {
        mFbAuth.signUp(
                email,
                password,
                fullName,
                userName,
                completeListener);
    }

    public void signOut() {
        mFbAuth.signOut();
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user == null) {
            int i = mUserDao.delete();
            int j = mNearByAtmDao.deleteAll();
            int k = mCheckDao.deleteAll();

            Log.e(TAG, "onAuthStateChanged: " + i + " | " + j + " | " + k);
            INSTANCE = null;
            mFbAuth.removeAuthStateListener(this);
        }
    }

    public void registerErrorIssuer(DisplayError displayError) {
        mDisplayError = displayError;
    }

    public void setCurrentUserLocation(LatLng latLng) {
        mUserLocation = latLng;
    }

    public interface DisplayError {
        void issueError(String error);
    }
}

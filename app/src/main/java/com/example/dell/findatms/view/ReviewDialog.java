package com.example.dell.findatms.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.dell.findatms.R;

public class ReviewDialog extends DialogFragment {

    private DialogButtonClickEvent mEvent;

    public static ReviewDialog newInstance() {
        ReviewDialog dialog = new ReviewDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);

        return dialog;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.dialog_review, null);

        final EditText reviewEdt = dialogView.findViewById(R.id.edt_review);
        TextView done = dialogView.findViewById(R.id.text_done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEvent.onDoneClicked(reviewEdt.getText().toString());
                dismiss();
            }
        });
        TextView cancel = dialogView.findViewById(R.id.text_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        builder.setView(dialogView).setMessage("Review");
        return builder.create();
    }

    public void setEvent(DialogButtonClickEvent event) {
        mEvent = event;
    }

    public interface DialogButtonClickEvent {
        void onDoneClicked(String text);
    }
}

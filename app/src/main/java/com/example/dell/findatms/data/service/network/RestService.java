package com.example.dell.findatms.data.service.network;

import com.example.dell.findatms.data.service.network.direction.DirectionApiResponse;
import com.example.dell.findatms.data.service.network.distance.DistanceMatrixApiResponse;
import com.example.dell.findatms.data.service.network.place.PlaceApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface RestService {
    @GET()
    Call<PlaceApiResponse> getNearByResponse(@Url String path);

    @GET()
    Call<DistanceMatrixApiResponse> getDistanceResponse(@Url String path);

    @GET()
    Call<DirectionApiResponse> getDirectionResponse(@Url String path);
}

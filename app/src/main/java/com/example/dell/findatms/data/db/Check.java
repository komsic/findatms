package com.example.dell.findatms.data.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.example.dell.findatms.data.db.utils.CheckConverter;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "check_table")
@TypeConverters(CheckConverter.class)
public class Check {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "atm_id")
    private String atmId;

    @ColumnInfo(name = "atm_name")
    private String atmName;

    private List<CheckTime> checks;

    public Check() {
        checks = new ArrayList<>();
    }

    @Ignore
    public Check(@NonNull String atmId, String atmName) {
        this.atmId = atmId;
        this.atmName = atmName;
        checks = new ArrayList<>();
    }

    @NonNull
    public String getAtmId() {
        return atmId;
    }

    public void setAtmId(@NonNull String atmId) {
        this.atmId = atmId;
    }

    public String getAtmName() {
        return atmName;
    }

    public void setAtmName(String atmName) {
        this.atmName = atmName;
    }

    public List<CheckTime> getChecks() {
        return checks;
    }

    public void setChecks(List<CheckTime> checks) {
        this.checks = checks;
    }

    @Ignore
    public void addCheckIn(CheckTime checkTime) {
        checks.add(checkTime);
    }
}

package com.example.dell.findatms.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.dell.findatms.R;
import com.example.dell.findatms.data.db.NearByAtm;
import com.example.dell.findatms.viewmodel.MarkerDetailViewModel;
import com.example.dell.findatms.viewmodel.ViewModelFactory;

public class MarkerDetailFragment extends Fragment {
    private static final String TAG = "MarkerDetailFragment";
    private static final String KEY_EXTRA_ID = "ID";

    private OnButtonClicked mOnButtonClicked;

    private MarkerDetailViewModel mViewModel;
    private RatingBar mRatingBar;
    private TextView mNameTxt;
    private TextView mCheckInTxt;
    private TextView mDrivingTxt;
    private TextView mRatingTxt;
    private TextView mDirectionTxt;
    private TextView mReviewTxt;

    public MarkerDetailFragment() {
    }

    public static MarkerDetailFragment newInstance(String id) {
        MarkerDetailFragment dialog = new MarkerDetailFragment();
        Bundle args = new Bundle();
        args.putString(KEY_EXTRA_ID, id);
        dialog.setArguments(args);

        return dialog;
    }

    @NonNull
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.frag_marker_detail, null);

        mNameTxt = rootView.findViewById(R.id.text_name);
        mCheckInTxt = rootView.findViewById(R.id.text_check_in);
        mDrivingTxt = rootView.findViewById(R.id.text_driving);
        mRatingTxt = rootView.findViewById(R.id.text_rating);
        mRatingBar = rootView.findViewById(R.id.rating_bar);
        mReviewTxt = rootView.findViewById(R.id.text_review);
        mDirectionTxt = rootView.findViewById(R.id.text_direction);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ViewModelFactory viewModelFactory = new ViewModelFactory();
        mViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(MarkerDetailViewModel.class);

        if (getArguments() != null) {
            String id = getArguments().getString(KEY_EXTRA_ID);

            if (id != null && id.length() > 0) {
                Log.e(TAG, "onActivityCreated: " + id);
                mViewModel.getNearByAtm(id).observe(this, new Observer<NearByAtm>() {
                    @Override
                    public void onChanged(@Nullable final NearByAtm atm) {
                        Log.e(TAG, "onChanged: ");
                        if (atm != null) {
                            Log.e(TAG, "onChanged: " + atm.getName());
                            mNameTxt.setText(atm.getName());
                            mCheckInTxt.setText(String.valueOf(atm.getCheck()));
                            mDrivingTxt.setText(atm.getDrivingTime());
                            mRatingBar.setRating(atm.getRate());
                            mRatingTxt.setText(String.valueOf(atm.getRate()));

                            mDirectionTxt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mOnButtonClicked.onDirectionClicked(atm);
                                }
                            });

                            mReviewTxt.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    mOnButtonClicked.onReviewClicked(atm.getId());
                                }
                            });
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onDetach() {
        mOnButtonClicked = null;
        super.onDetach();
    }

    public void setOnButtonClicked(OnButtonClicked onButtonClicked) {
        mOnButtonClicked = onButtonClicked;
    }

    public interface OnButtonClicked {
        void onReviewClicked(String id);

        void onDirectionClicked(NearByAtm atm);
    }
}

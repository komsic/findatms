package com.example.dell.findatms.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.dell.findatms.R;

import static com.example.dell.findatms.view.SplashActivity.ADD_NEW_EXTRA;
import static com.example.dell.findatms.view.SplashActivity.SIGN_IN_EXTRA;

public class LogInActivity extends AppCompatActivity {

    public static Intent getStartIntent(Context context) {
        return new Intent(context, LogInActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        setUp();
    }

    protected void setUp() {
        int intentExtra = getIntent().getIntExtra(SplashActivity.FRAGMENT_EXTRA, 1);

        if (intentExtra > 0) {
            attachFragment(intentExtra);
        }
    }

    public void attachFragment(int whichFragment) {
        Fragment fragment = getFragment(whichFragment);

        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, fragment);
        transaction.commit();
    }

    private Fragment getFragment(int whichFragment) {
        switch (whichFragment) {
            case SIGN_IN_EXTRA:
                return SignInFragment.newInstance();
            case ADD_NEW_EXTRA:
                return NewUserFragment.newInstance();
            default:
                throw new UnsupportedOperationException("Fragment invalid");
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(SplashActivity.getStartIntent(this));
        finish();
    }
}

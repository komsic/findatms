package com.example.dell.findatms.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dell.findatms.R;
import com.example.dell.findatms.viewmodel.SignInViewModel;
import com.example.dell.findatms.viewmodel.ViewModelFactory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;

public class SignInFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "SignInFragment";

    private TextInputEditText passwordEditText;
    private TextInputEditText emailEditText;
    private TextInputLayout emailInputLayout;
    private TextInputLayout passwordInputLayout;
    private ProgressBar mProgressBar;

    private SignInViewModel mViewModel;

    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment newInstance() {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);

        mProgressBar = rootView.findViewById(R.id.progress_bar);

        passwordEditText = rootView.findViewById(R.id.password_text_edit);
        emailEditText = rootView.findViewById(R.id.email_text_edit);
        emailInputLayout = rootView.findViewById(R.id.email_text_input);
        passwordInputLayout = rootView.findViewById(R.id.password_text_input);

        TextView loginTextView = rootView.findViewById(R.id.text_login);
        loginTextView.setOnClickListener(this);
        TextView forgotPasswordTextView = rootView.findViewById(R.id.text_forgot_password);
        forgotPasswordTextView.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ViewModelFactory viewModelFactory = new ViewModelFactory();
        mViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(SignInViewModel.class);
    }

    @Override
    public void onDestroyView() {
        mProgressBar.setVisibility(View.GONE);
        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        String email = emailEditText.getText().toString();
        switch (view.getId()) {
            case R.id.text_forgot_password:
                if (email.length() > 4 && email.contains("@")) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    issueForgottenEmailError();
                } else {
                    Toast.makeText(getActivity(), "Please enter a valid email",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.text_login:
                if (email.length() < 4 || !email.contains("@") ||
                        passwordEditText.getText().length() <= 4) {
                    Log.e(TAG, "onClick: inside if");
                    issueError(true);
                    return;
                } else {
                    issueError(false);
                    Log.e(TAG, "onClick: outside if");
                    String password = passwordEditText.getText().toString();
                    mProgressBar.setVisibility(View.VISIBLE);

                    OnCompleteListener<AuthResult> completeListener = new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                openMainActivity();
                            } else {
                                issueError(true);
                            }
                        }
                    };
                    mViewModel.signIn(email, password, completeListener);
                }
                break;
        }
    }

    public void issueForgottenEmailError() {
        mProgressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), "Email sent", Toast.LENGTH_SHORT).show();
    }

    public void issueError(boolean status) {
        if (status) {
            mProgressBar.setVisibility(View.GONE);
            emailInputLayout.setError("Email is not valid");
            passwordInputLayout.setError("Or password is incorrect");
        } else {
            emailInputLayout.setError(null);
            passwordInputLayout.setError(null);
        }
    }

    private void openMainActivity() {
        Intent intent = MainActivity.getStartIntent(getActivity());
        startActivity(intent);
    }
}

package com.example.dell.findatms.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.dell.findatms.R;
import com.example.dell.findatms.data.db.Review;

import java.util.ArrayList;
import java.util.List;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewsVH> {

    private List<Review> mReviews;

    ReviewsAdapter() {
        mReviews = new ArrayList<>();
    }

    @NonNull
    @Override
    public ReviewsVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.review_item,
                viewGroup, false);
        return new ReviewsVH(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewsVH holder, int position) {
        Review review = mReviews.get(position);

        holder.name.setText(review.getUserId());

        double rating = review.getRating();
        holder.ratingBar.setRating((float) rating);

        holder.reviewMessage.setText(review.getMessage());
    }

    @Override
    public int getItemCount() {
        return mReviews == null ? 0 : mReviews.size();
    }

    public void swap(List<Review> reviews) {
        mReviews = reviews;
        notifyDataSetChanged();
    }

    class ReviewsVH extends RecyclerView.ViewHolder {

        TextView name;
        TextView reviewMessage;
        RatingBar ratingBar;
        ImageView userPic;

        public ReviewsVH(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.text_name);
            reviewMessage = itemView.findViewById(R.id.text_review);
            ratingBar = itemView.findViewById(R.id.rating_bar);
            userPic = itemView.findViewById(R.id.user_pic);
        }
    }
}

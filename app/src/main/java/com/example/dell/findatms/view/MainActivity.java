package com.example.dell.findatms.view;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.dell.findatms.R;
import com.example.dell.findatms.data.AppRepository;
import com.example.dell.findatms.data.db.NearByAtm;
import com.example.dell.findatms.util.Geofencing;
import com.example.dell.findatms.view.MarkerDetailFragment.OnButtonClicked;
import com.example.dell.findatms.viewmodel.MainViewModel;
import com.example.dell.findatms.viewmodel.ViewModelFactory;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MainActivity";
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 99;

    public static final String KEY_EXTRA_IS_SIGN_IN = "IS_SIGN_IN";

    //constants to control screen
    private static final int SEARCH = 1;
    private static final int LIST_MARKER = 2;
    private static final int MARKER_DETAIL = 3;
    private static final int REVIEW = 4;
    private static final int DIRECTION = 5;
    private int whichScreen;
    private FloatingActionButton mFab;

    private GoogleMap mMap;
    private FrameLayout reviewFrag;
    private FrameLayout mMarkerDetailLayout;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLastKnownLocation;

    private LocationRequest mLocationRequest;
    private MainViewModel mViewModel;
    private boolean mShowNearByAtms;
    private boolean hasRemovedMarkerCalled;
    private HashMap<String, Marker> mMarkers = new HashMap<>();
    private Geofencing mGeofencing;

    private ReviewFragment mReviewFragment;

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                Location location = locationList.get(locationList.size() - 1);
                mLastKnownLocation = location;

                LatLng latLng = new LatLng(mLastKnownLocation.getLatitude(),
                        mLastKnownLocation.getLongitude());
                mViewModel.setCurrentUserLocation(latLng);
                CameraPosition cp = CameraPosition.builder()
                        .target(latLng)
                        .zoom(15)
                        .build();
                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cp));
            }
        }
    };

    private OnButtonClicked onButtonClicked = new OnButtonClicked() {
        @Override
        public void onReviewClicked(String id) {
            reviewFrag = findViewById(R.id.review);
            String tag = "ReviewFragment";
            mReviewFragment = (ReviewFragment) getSupportFragmentManager().findFragmentByTag(tag);
            FragmentTransaction transaction =
                    getSupportFragmentManager().beginTransaction();
            if (mReviewFragment == null) {
                mReviewFragment = ReviewFragment.newInstance(id);
                transaction.add(R.id.review, mReviewFragment, tag);
            } else {
                mReviewFragment = ReviewFragment.newInstance(id);
                transaction.replace(R.id.review, mReviewFragment, tag);
            }

            transaction.commit();
            whichScreen = REVIEW;
            reviewFrag.setVisibility(View.VISIBLE);
        }

        @Override
        public void onDirectionClicked(final NearByAtm atm) {
            removeAllNearByMarker(atm);
            mViewModel.getPathToAtm(atm).observe(MainActivity.this, new Observer<String>() {
                @Override
                public void onChanged(@Nullable String path) {
                    if (path != null) {
                        List<LatLng> decodedPath = PolyUtil.decode(path);
                        mMap.addPolyline(new PolylineOptions()
                                .jointType(JointType.ROUND)
                                .addAll(decodedPath));

                        mMarkerDetailLayout.setVisibility(View.GONE);

                        mGeofencing.updateGeofence(atm);
                        mGeofencing.registerGeofence(atm.getName(), atm.getId());
                        whichScreen = DIRECTION;
                    } else {
                        mViewModel.getPathToAtmFromService(atm);
                    }
                }
            });
        }
    };
    private GoogleMap.OnMarkerClickListener onMarkerClickListener =
            new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {

                    NearByAtm atm = (NearByAtm) marker.getTag();

                    if (atm != null) {

                        String fragmentTag = "MarkerDetailFragment";
                        FrameLayout frameLayout = findViewById(R.id.pop_up);
                        MarkerDetailFragment dialog =
                                (MarkerDetailFragment) getSupportFragmentManager()
                                        .findFragmentByTag(fragmentTag);
                        FragmentTransaction transaction =
                                getSupportFragmentManager().beginTransaction();
                        if (dialog == null) {
                            dialog = MarkerDetailFragment.newInstance(atm.getId());
                            dialog.setOnButtonClicked(onButtonClicked);
                            transaction.add(R.id.pop_up, dialog, fragmentTag);
                        } else {
                            dialog = MarkerDetailFragment.newInstance(atm.getId());
                            dialog.setOnButtonClicked(onButtonClicked);
                            transaction.replace(R.id.pop_up, dialog, fragmentTag);
                        }
                        transaction.commit();
                        frameLayout.setVisibility(View.VISIBLE);
                    }

                    return false;
                }
            };

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        mGeofencing = new Geofencing(this);
        whichScreen = SEARCH;

        ViewModelFactory viewModelFactory = new ViewModelFactory();
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);

        boolean isFirstSignIn = getIntent().getBooleanExtra(KEY_EXTRA_IS_SIGN_IN, false);
        mViewModel.initializeUser(isFirstSignIn);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        mMarkerDetailLayout = findViewById(R.id.pop_up);

        mFab = findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLastKnownLocation != null) {
                    mShowNearByAtms = true;
                    mViewModel.searchNearByAtms(mLastKnownLocation.getLatitude(),
                            mLastKnownLocation.getLongitude());
                    Log.e(TAG, "onClick: mFab clicked");
                }
                view.setVisibility(View.GONE);
            }
        });

        mViewModel.getNearByAtm().observe(this, new Observer<List<NearByAtm>>() {
            @Override
            public void onChanged(@Nullable List<NearByAtm> nearByAtms) {
                if (!hasRemovedMarkerCalled && mShowNearByAtms && nearByAtms != null) {
                    whichScreen = LIST_MARKER;
                    addNearbyMarker(nearByAtms);
                } else {
                    Toast.makeText(MainActivity.this,
                            "Please Click Search Button To Get Started",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        mViewModel.registerErrorIssuer(new AppRepository.DisplayError() {
            @Override
            public void issueError(String error) {
                Toast.makeText(MainActivity.this, "Error processing: " + error,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void addNearbyMarker(List<NearByAtm> nearByAtms) {

        for (final NearByAtm atm : nearByAtms) {
            Marker marker = addNearByMarker(atm);
            mMarkers.put(atm.getId(), marker);
        }
    }

    private Marker addNearByMarker(NearByAtm atm) {
        LatLng latLng = new LatLng(atm.getLat(), atm.getLng());
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(atm.getName())
                .snippet(atm.getDistanceToCurrentPlace())
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_local_atm)));
        marker.setTag(atm);

        mMap.setOnMarkerClickListener(onMarkerClickListener);

        return marker;
    }

    @Override
    public void onBackPressed() {
        switch (whichScreen) {
            case SEARCH:
                super.onBackPressed();
                break;
            case LIST_MARKER:
                removeAllNearByMarker(null);
                mFab.setVisibility(View.VISIBLE);
                whichScreen = SEARCH;
                break;
            case MARKER_DETAIL:
                super.onBackPressed();
                break;
            case REVIEW:
                reviewFrag.setVisibility(View.GONE);
                whichScreen = MARKER_DETAIL;
                break;
            case DIRECTION:
                mMap.clear();
                whichScreen = SEARCH;
                mFab.setVisibility(View.VISIBLE);
                break;
            default:
                super.onBackPressed();

        }
    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mFusedLocationProviderClient != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                        mMap.getUiSettings().setMyLocationButtonEnabled(true);
                    }
                } else {
                    Toast.makeText(this, "Permission Denied",
                            Toast.LENGTH_SHORT).show();
                }

                return;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(120000);
        mLocationRequest.setFastestInterval(120000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);


        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.myLooper());

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    private void removeAllNearByMarker(NearByAtm atm) {
        for (Marker marker : mMarkers.values()) {
            marker.remove();
        }
        hasRemovedMarkerCalled = true;

        if (atm != null) {
            addNearByMarker(atm);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out:
                mViewModel.signOut();
                openSplashActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openSplashActivity() {
        startActivity(SplashActivity.getStartIntent(this));
        finish();
    }
}
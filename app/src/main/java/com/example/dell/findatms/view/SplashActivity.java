package com.example.dell.findatms.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.dell.findatms.R;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String FRAGMENT_EXTRA = "FRAGMENT_EXTRA";
    public static final int SIGN_IN_EXTRA = 1;
    public static final int ADD_NEW_EXTRA = 2;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, SplashActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setUp();
    }

    protected void setUp() {
        TextView login = findViewById(R.id.text_login);
        TextView signUp = findViewById(R.id.text_sign_up);

        login.setOnClickListener(this);
        signUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.text_login:
                openLoginActivity(SIGN_IN_EXTRA);
//                openMainAct();
                break;
            case R.id.text_sign_up:
                openLoginActivity(ADD_NEW_EXTRA);
                break;
            default:
                throw new UnsupportedOperationException("Not a valid view");
        }
    }

    private void openMainAct() {
        Intent intent = MainActivity.getStartIntent(this);
        startActivity(intent);
        finish();
    }

    public void openLoginActivity(int whichFragment) {
        Intent intent = LogInActivity.getStartIntent(SplashActivity.this);
        intent.putExtra(FRAGMENT_EXTRA, whichFragment);
        startActivity(intent);
        finish();
    }
}

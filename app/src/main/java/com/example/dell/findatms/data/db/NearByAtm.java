package com.example.dell.findatms.data.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.example.dell.findatms.data.db.utils.ReviewConverter;

import java.util.ArrayList;

@Entity
@TypeConverters(ReviewConverter.class)
public class NearByAtm {

    @PrimaryKey
    @NonNull
    private String id;

    private String name;
    private double lat;
    private double lng;

    private ArrayList<Review> reviews;

    private int check;
    private String path;

    @ColumnInfo(name = "distance_to_current_place")
    private String distanceToCurrentPlace;

    @ColumnInfo(name = "driving_time")
    private String drivingTime;

    @Ignore
    private float rate;

    @Ignore
    private boolean hasRateBeenCalc;

    public NearByAtm() {
    }

    @Ignore
    public NearByAtm(@NonNull String id, String name, double lat, double lng, int check) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.check = check;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public ArrayList<Review> getReviews() {
        return reviews;
    }

    public void setReviews(ArrayList<Review> reviews) {
        this.reviews = reviews;
    }

    public float getRate() {
        if (!hasRateBeenCalc) {
            setRate();
            hasRateBeenCalc = true;
        }
        return rate;
    }

    public void setRate() {
        float total = 0;
        if (reviews != null && reviews.size() > 0) {
            for (Review review : reviews) {
                total += review.getRating();
            }

            rate = total / reviews.size();
        }
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDistanceToCurrentPlace() {
        return distanceToCurrentPlace;
    }

    public void setDistanceToCurrentPlace(String distanceToCurrentPlace) {
        this.distanceToCurrentPlace = distanceToCurrentPlace;
    }

    public String getDrivingTime() {
        return drivingTime;
    }

    public void setDrivingTime(String drivingTime) {
        this.drivingTime = drivingTime;
    }
}

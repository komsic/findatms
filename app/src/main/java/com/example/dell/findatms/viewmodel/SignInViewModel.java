package com.example.dell.findatms.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.example.dell.findatms.data.AppRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;

public class SignInViewModel extends ViewModel {
    private AppRepository mRepository;

    public SignInViewModel(AppRepository repository) {
        mRepository = repository;
    }

    public void signIn(String email, String password,
                       OnCompleteListener<AuthResult> completeListener) {
        mRepository.signIn(email.trim(), password, completeListener);
    }
}

package com.example.dell.findatms.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.example.dell.findatms.R;
import com.example.dell.findatms.data.db.NearByAtm;
import com.example.dell.findatms.viewmodel.ReviewViewModel;
import com.example.dell.findatms.viewmodel.ViewModelFactory;

public class ReviewFragment extends Fragment {

    private static final String TAG = "ReviewFragment";
    private static final String KEY_EXTRA_ID = "ID";
    private ReviewViewModel mViewModel;
    private ViewModelFactory mViewModelFactory;
    private ReviewsAdapter mAdapter;
    private RatingBar mRatingBar;
    private String mId;

    private ReviewDialog.DialogButtonClickEvent mButtonClickEvent =
            new ReviewDialog.DialogButtonClickEvent() {
                @Override
                public void onDoneClicked(String text) {
                    mViewModel.setReview(text, mRatingBar.getRating(), mId);
                }
            };

    public ReviewFragment() {
        // Required empty public constructor
    }

    public static ReviewFragment newInstance(String id) {
        ReviewFragment fragment = new ReviewFragment();
        Bundle args = new Bundle();
        args.putString(KEY_EXTRA_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.reviews, container, false);

        mRatingBar = rootView.findViewById(R.id.rating_bar);

        RecyclerView recyclerView = rootView.findViewById(R.id.rc);
        mAdapter = new ReviewsAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(mAdapter);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModelFactory = new ViewModelFactory();
        mViewModel = ViewModelProviders.of(this, mViewModelFactory)
                .get(ReviewViewModel.class);
        if (getArguments() != null) {
            mId = getArguments().getString(KEY_EXTRA_ID);
            if (mId != null) {
                mViewModel.getReviews(mId).observe(this, new Observer<NearByAtm>() {
                    @Override
                    public void onChanged(@Nullable NearByAtm atm) {
                        if (atm != null) {
                            mAdapter.swap(atm.getReviews());
                        }
                    }
                });
            }
        }

        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                ReviewDialog dialog = ReviewDialog.newInstance();
                dialog.setEvent(mButtonClickEvent);
                if (getFragmentManager() != null) {
                    dialog.show(getFragmentManager(), "123");
                }
            }
        });
    }
}

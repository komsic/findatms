package com.example.dell.findatms.util;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.dell.findatms.FindAtmApp;
import com.example.dell.findatms.data.AppRepository;
import com.example.dell.findatms.data.db.CheckTime;
import com.example.dell.findatms.data.db.utils.Constants;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import static com.example.dell.findatms.data.db.utils.Constants.KEY_PREF_TIME_IN;

public class GeofenceIntentService extends IntentService {

    private static final String TAG = "GeofenceIntentService";

    public GeofenceIntentService() {
        super("GeofenceIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String atmId = intent.getStringExtra(Geofencing.KEY_EXTRA_ATM_ID);
            String atmName = intent.getStringExtra(Geofencing.KEY_EXTRA_ATM_NAME);
            GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
            if (geofencingEvent.hasError()) {
                Log.e(TAG, "error: " + geofencingEvent.getErrorCode());
                return;
            }

            int geofenceTrans = geofencingEvent.getGeofenceTransition();
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
            int lastGeofenceTrans = pref.getInt(Constants.KEY_PREF_LAST_GEOFENCE_TRANSITION, 0);
            long timeIn = pref.getLong(KEY_PREF_TIME_IN, 0);

            if (geofenceTrans == Geofence.GEOFENCE_TRANSITION_EXIT &&
                    lastGeofenceTrans == Geofence.GEOFENCE_TRANSITION_ENTER) {
                long timeOut = System.currentTimeMillis();
                if (timeIn > 0) {
                    if (timeOut - timeIn >= 60000) {
                        AppRepository repo = FindAtmApp.getRepository();
                        repo.addCheckIn(new CheckTime(timeIn, timeOut), atmId, atmName);
                    }
                }
            } else if (geofenceTrans == Geofence.GEOFENCE_TRANSITION_ENTER) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt(Constants.KEY_PREF_LAST_GEOFENCE_TRANSITION, geofenceTrans);
                editor.putLong(KEY_PREF_TIME_IN, System.currentTimeMillis());
                editor.apply();
            }
        }
    }
}

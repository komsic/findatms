package com.example.dell.findatms;

import android.app.Application;

import com.example.dell.findatms.data.AppRepository;

public class FindAtmApp extends Application {

    private static AppRepository mRepository;

    public static AppRepository getRepository() {
        return mRepository;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mRepository = AppRepository.getInstance(getApplicationContext());
    }
}

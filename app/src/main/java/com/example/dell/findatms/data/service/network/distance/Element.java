package com.example.dell.findatms.data.service.network.distance;

import com.example.dell.findatms.data.service.network.common.Metric;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Element {
    @SerializedName("distance")
    @Expose
    private Metric distance;
    @SerializedName("duration")
    @Expose
    private Metric duration;
    @SerializedName("status")
    @Expose
    private String status;

    public Metric getDistance() {
        return distance;
    }

    public void setDistance(Metric distance) {
        this.distance = distance;
    }

    public Metric getDuration() {
        return duration;
    }

    public void setDuration(Metric duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

package com.example.dell.findatms.data.db;

import android.arch.persistence.room.Ignore;

public class CheckTime {

    private long checkIn;
    private long checkOut;

    public CheckTime() {
    }

    public CheckTime(long checkIn, long checkOut) {
        this.checkIn = checkIn;
        this.checkOut = checkOut;
    }

    @Ignore


    public long getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(long checkIn) {
        this.checkIn = checkIn;
    }

    public long getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(long checkOut) {
        this.checkOut = checkOut;
    }
}

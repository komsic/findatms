package com.example.dell.findatms.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.dell.findatms.data.AppRepository;
import com.example.dell.findatms.data.db.NearByAtm;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class MainViewModel extends ViewModel {

    private AppRepository mRepository;
    private LiveData<List<NearByAtm>> mNearByAtm;

    MainViewModel(AppRepository repository) {
        mRepository = repository;
        mNearByAtm = mRepository.getNearByAtms();
    }

    public LiveData<List<NearByAtm>> getNearByAtm() {
        return mNearByAtm;
    }

    public void searchNearByAtms(double latitude, double longitude) {
        mRepository.searchNearByAtms(latitude, longitude);
    }

    public LiveData<String> getPathToAtm(NearByAtm atm) {
        return mRepository.getPathToAtm(atm.getId());
    }

    public void getPathToAtmFromService(NearByAtm atm) {
        mRepository.getPathToAtmFromService(atm);
    }

    public void initializeUser(boolean isFirstSignIn) {
        mRepository.initializeUser(isFirstSignIn);
    }

    public void signOut() {
        mRepository.signOut();
    }

    public void registerErrorIssuer(AppRepository.DisplayError displayError) {
        mRepository.registerErrorIssuer(displayError);
    }

    public void setCurrentUserLocation(LatLng latLng) {
        mRepository.setCurrentUserLocation(latLng);
    }
}

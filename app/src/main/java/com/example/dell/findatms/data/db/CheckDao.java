package com.example.dell.findatms.data.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import com.example.dell.findatms.data.db.utils.CheckConverter;

import java.util.List;

@Dao
@TypeConverters(CheckConverter.class)
public interface CheckDao {

    @Query("SELECT * FROM check_table")
    LiveData<List<Check>> findAllCheckIns();

    @Query("SELECT * FROM check_table WHERE atm_id = :atmId")
    Check findCheckInByAtmId(String atmId);

    @Query("UPDATE check_table "
            + "SET checks = :checkTimes "
            + "WHERE atm_id = :atmId")
    int updateCheckInList(List<CheckTime> checkTimes, String atmId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertCheckIn(Check check);

    @Query("DELETE FROM check_table")
    int deleteAll();
}


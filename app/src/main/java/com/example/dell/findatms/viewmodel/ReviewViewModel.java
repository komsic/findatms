package com.example.dell.findatms.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;

import com.example.dell.findatms.data.AppRepository;
import com.example.dell.findatms.data.db.NearByAtm;

public class ReviewViewModel extends ViewModel {

    private AppRepository mRepository;

    public ReviewViewModel(AppRepository repository) {
        mRepository = repository;
    }

    public LiveData<NearByAtm> getReviews(String id) {
        return mRepository.getNearByAtmById(id);
    }

    public void setReview(String text, float rating, String atmId) {
        mRepository.addUserReview(text, rating, atmId);
    }
}

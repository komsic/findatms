package com.example.dell.findatms.data.service.firebase;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.dell.findatms.data.AppRepository;
import com.example.dell.findatms.data.db.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FbAuth {
    private static final String TAG = "FbAuth";
    private static FbAuth sInstance;
    private String mUserName;
    private String mUid;

    private FirebaseAuth mAuth;

    private FbAuth() {
        mAuth = FirebaseAuth.getInstance();
    }

    public static FbAuth getInstance() {
        if (sInstance == null) {
            sInstance = new FbAuth();
        }
        return sInstance;
    }

    public void signIn(String email, String password,
                       OnCompleteListener<AuthResult> completeListener) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(completeListener);
    }

    public void signUp(String email, String password, String fullName,
                       String userName,
                       OnCompleteListener<AuthResult> completeListener) {

        mUserName = userName;
        mAuth.createUserWithEmailAndPassword(email, password).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                Log.e(TAG, "onFailure: " + e.getMessage());
            }
        })
                .addOnCompleteListener(completeListener);
    }

    public User getUser() {
        FirebaseUser user = mAuth.getCurrentUser();
        User currentUser = null;
        if (user != null) {
            mUid = user.getUid();
            String email = user.getEmail();
            Uri imageUri = user.getPhotoUrl();
            String imageUrl = null;
            if (imageUri != null) {
                imageUrl = imageUri.toString();
            }
            currentUser = new User(mUid, email, imageUrl, mUserName);
        }

        return currentUser;
    }

    public String getUid() {
        return mUid;
    }

    public void signOut() {
        mAuth.signOut();
    }

    public void addAuthStateListener(FirebaseAuth.AuthStateListener listener) {
        mAuth.addAuthStateListener(listener);
    }

    public void removeAuthStateListener(FirebaseAuth.AuthStateListener listener) {
        mAuth.removeAuthStateListener(listener);
    }
}

package com.example.dell.findatms.data.service.network;

import android.support.annotation.NonNull;
import android.util.Log;

import com.example.dell.findatms.data.db.NearByAtm;
import com.example.dell.findatms.data.service.network.direction.DirectionApiResponse;
import com.example.dell.findatms.data.service.network.distance.DistanceMatrixApiResponse;
import com.example.dell.findatms.data.service.network.place.PlaceApiResponse;
import com.example.dell.findatms.data.service.network.place.Result;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Remote {
    private static final String TAG = "Remote";

    private RestService mRestService;
    private PlaceApiResponse mPlaceApiResponse;
    private DistanceMatrixApiResponse mDistanceMatrixApiResponse;
    private DirectionApiResponse mDirectionApiResponse;
    private String jsonDir = "{\n" +
            "   \"geocoded_waypoints\" : [\n" +
            "      {\n" +
            "         \"geocoder_status\" : \"OK\",\n" +
            "         \"place_id\" : \"ChIJRZCfYSn1OxARcXfIrw8_IvE\",\n" +
            "         \"types\" : [ \"beauty_salon\", \"establishment\", \"point_of_interest\" ]\n" +
            "      },\n" +
            "      {\n" +
            "         \"geocoder_status\" : \"OK\",\n" +
            "         \"place_id\" : \"ChIJVd8liR71OxARTKQ6TpEcRmA\",\n" +
            "         \"types\" : [ \"church\", \"establishment\", \"place_of_worship\", \"point_of_interest\" ]\n" +
            "      }\n" +
            "   ],\n" +
            "   \"routes\" : [\n" +
            "      {\n" +
            "         \"bounds\" : {\n" +
            "            \"northeast\" : {\n" +
            "               \"lat\" : 6.4389734,\n" +
            "               \"lng\" : 3.4256133\n" +
            "            },\n" +
            "            \"southwest\" : {\n" +
            "               \"lat\" : 6.4283608,\n" +
            "               \"lng\" : 3.421342\n" +
            "            }\n" +
            "         },\n" +
            "         \"copyrights\" : \"Map data ©2018 Google\",\n" +
            "         \"legs\" : [\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"2.2 km\",\n" +
            "                  \"value\" : 2191\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"7 mins\",\n" +
            "                  \"value\" : 427\n" +
            "               },\n" +
            "               \"end_address\" : \"56 Adeola Odeku St, Victoria Island, Lagos, Nigeria\",\n" +
            "               \"end_location\" : {\n" +
            "                  \"lat\" : 6.4283608,\n" +
            "                  \"lng\" : 3.422003\n" +
            "               },\n" +
            "               \"start_address\" : \"Keystone Bank Crescent, Adeyemo Alakija St, Victoria Island, Lagos, Nigeria\",\n" +
            "               \"start_location\" : {\n" +
            "                  \"lat\" : 6.4352928,\n" +
            "                  \"lng\" : 3.4247218\n" +
            "               },\n" +
            "               \"steps\" : [\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.2 km\",\n" +
            "                        \"value\" : 162\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"1 min\",\n" +
            "                        \"value\" : 42\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 6.436711499999999,\n" +
            "                        \"lng\" : 3.4250348\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Head \\u003cb\\u003enorth\\u003c/b\\u003e on \\u003cb\\u003eAdeyemo Alakija St\\u003c/b\\u003e\\u003cdiv style=\\\"font-size:0.9em\\\"\\u003ePass by MARIGOLD TRAVEL AGENCY (on the right)\\u003c/div\\u003e\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"q{gf@o{{S]C_AKq@Kc@Gc@I{@MGA\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 6.4352928,\n" +
            "                        \"lng\" : 3.4247218\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.3 km\",\n" +
            "                        \"value\" : 251\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"1 min\",\n" +
            "                        \"value\" : 68\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 6.438849299999999,\n" +
            "                        \"lng\" : 3.4255074\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Continue onto \\u003cb\\u003eAdeyemo Alakija Street\\u003c/b\\u003e\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"mdhf@m}{S[I_A_@gA[k@QOCy@QKAMAQAO?UB_AN\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 6.436711499999999,\n" +
            "                        \"lng\" : 3.4250348\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.3 km\",\n" +
            "                        \"value\" : 284\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"1 min\",\n" +
            "                        \"value\" : 44\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 6.4380096,\n" +
            "                        \"lng\" : 3.4232208\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Turn \\u003cb\\u003eleft\\u003c/b\\u003e onto \\u003cb\\u003eLekki - Epe Expy\\u003c/b\\u003e/\\u003cb\\u003eOzumba Mbadiwe Rd\\u003c/b\\u003e\\u003cdiv style=\\\"font-size:0.9em\\\"\\u003ePass by Law School Bus Stop (on the right)\\u003c/div\\u003e\",\n" +
            "                     \"maneuver\" : \"turn-left\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"yqhf@m`|SWHNh@`@tA\\\\dA|@hCp@nB\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 6.438849299999999,\n" +
            "                        \"lng\" : 3.4255074\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"1.0 km\",\n" +
            "                        \"value\" : 1026\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"2 mins\",\n" +
            "                        \"value\" : 148\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 6.4292205,\n" +
            "                        \"lng\" : 3.423906\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Exit on the \\u003cb\\u003eleft\\u003c/b\\u003e onto \\u003cb\\u003eAkin Adesola St\\u003c/b\\u003e toward \\u003cb\\u003eVictoria Island\\u003c/b\\u003e\\u003cdiv style=\\\"font-size:0.9em\\\"\\u003ePass by Medplus Pharmacy (on the right in 1.0&nbsp;km)\\u003c/div\\u003e\",\n" +
            "                     \"maneuver\" : \"ramp-left\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"qlhf@cr{SHTTM`@Q`@O^QVSTWRYNQFIFINId@MbAPpAL|@HbBFbA@fDCdPGhF?L?~BA\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 6.4380096,\n" +
            "                        \"lng\" : 3.4232208\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.3 km\",\n" +
            "                        \"value\" : 285\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"1 min\",\n" +
            "                        \"value\" : 43\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 6.4294804,\n" +
            "                        \"lng\" : 3.4213594\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Turn \\u003cb\\u003eright\\u003c/b\\u003e at Kia Motors onto \\u003cb\\u003eAdeola Odeku St\\u003c/b\\u003e\\u003cdiv style=\\\"font-size:0.9em\\\"\\u003ePass by Sterling Bank (on the right)\\u003c/div\\u003e\",\n" +
            "                     \"maneuver\" : \"turn-right\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"suff@mv{SBjCAfAAh@Ef@I~@E`@Ij@ShA\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 6.4292205,\n" +
            "                        \"lng\" : 3.423906\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"86 m\",\n" +
            "                        \"value\" : 86\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"1 min\",\n" +
            "                        \"value\" : 32\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 6.429232499999999,\n" +
            "                        \"lng\" : 3.4220112\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Make a \\u003cb\\u003eU-turn\\u003c/b\\u003e at \\u003cb\\u003eEletu Ogabi St\\u003c/b\\u003e\",\n" +
            "                     \"maneuver\" : \"uturn-left\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"gwff@of{SPBRuAJo@\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 6.4294804,\n" +
            "                        \"lng\" : 3.4213594\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  },\n" +
            "                  {\n" +
            "                     \"distance\" : {\n" +
            "                        \"text\" : \"0.1 km\",\n" +
            "                        \"value\" : 97\n" +
            "                     },\n" +
            "                     \"duration\" : {\n" +
            "                        \"text\" : \"1 min\",\n" +
            "                        \"value\" : 50\n" +
            "                     },\n" +
            "                     \"end_location\" : {\n" +
            "                        \"lat\" : 6.4283608,\n" +
            "                        \"lng\" : 3.422003\n" +
            "                     },\n" +
            "                     \"html_instructions\" : \"Turn \\u003cb\\u003eright\\u003c/b\\u003e at Facefact Cosmetics\",\n" +
            "                     \"maneuver\" : \"turn-right\",\n" +
            "                     \"polyline\" : {\n" +
            "                        \"points\" : \"uuff@qj{S\\\\?b@@r@?v@?\"\n" +
            "                     },\n" +
            "                     \"start_location\" : {\n" +
            "                        \"lat\" : 6.429232499999999,\n" +
            "                        \"lng\" : 3.4220112\n" +
            "                     },\n" +
            "                     \"travel_mode\" : \"DRIVING\"\n" +
            "                  }\n" +
            "               ],\n" +
            "               \"traffic_speed_entry\" : [],\n" +
            "               \"via_waypoint\" : []\n" +
            "            }\n" +
            "         ],\n" +
            "         \"overview_polyline\" : {\n" +
            "            \"points\" : \"q{gf@o{{S}AOuAS_BWc@KgC{@{@UeAS_@Ce@BwAXp@~BzAnEz@dCv@_@`Aa@l@k@r@_At@WtC^`DPjFA|[I@rEGpAO`B]tBPBRuAJo@\\\\?vA@v@?\"\n" +
            "         },\n" +
            "         \"summary\" : \"Akin Adesola St\",\n" +
            "         \"warnings\" : [],\n" +
            "         \"waypoint_order\" : []\n" +
            "      }\n" +
            "   ],\n" +
            "   \"status\" : \"OK\"\n" +
            "}";
    private String jsonDist = "{\n" +
            "   \"destination_addresses\" : [\n" +
            "      \"56 Adeola Odeku St, Victoria Island, Lagos, Nigeria\",\n" +
            "      \"19 Adeola Odeku St, Victoria Island, Lagos, Nigeria\",\n" +
            "      \"5 Idowu Taylor St, Victoria Island, Lagos, Nigeria\",\n" +
            "      \"3 Akin Adesola St, Victoria Island 100272, Lagos, Nigeria\",\n" +
            "      \"3 Akin Adesola St, Victoria Island 100272, Lagos, Nigeria\",\n" +
            "      \"Plot 1607 Adeola Hopewell, Victoria Island, Lagos, Lagos, Lagos, Nigeria\",\n" +
            "      \"Plot 1607 Adeola Hopewell, Victoria Island, Lagos, Lagos, Lagos, Nigeria\",\n" +
            "      \"Plot 1607 Adeola Hopewell, Victoria Island, Lagos, Lagos, Lagos, Nigeria\",\n" +
            "      \"Plot 1607 Adeola Hopewell, Victoria Island, Lagos, Lagos, Lagos, Nigeria\",\n" +
            "      \"18, 16 Adeola Hopewell St, Lagos, Nigeria\",\n" +
            "      \"14 Adeola Hopewell St, Victoria Island, Lagos, Nigeria\",\n" +
            "      \"27 Adetokunbo Ademola Street, Victoria Island, Lagos, Nigeria\",\n" +
            "      \"27 Adetokunbo Ademola Street, Victoria Island, Lagos, Nigeria\",\n" +
            "      \"27 Adetokunbo Ademola Street, Victoria Island, Lagos, Nigeria\",\n" +
            "      \"No. 233, Adeola Odeku St, Lagos, Nigeria\",\n" +
            "      \"218 Ajose Adeogun St, Victoria Island 100272, Lagos, Nigeria\",\n" +
            "      \"275 Ajose Adeogun St, Victoria Island, Lagos, Nigeria\",\n" +
            "      \"275 Ajose Adeogun St, Victoria Island 101241, Lagos, Nigeria\",\n" +
            "      \"1698 Oyin Jolayemi St, Victoria Island, Lagos, Nigeria\",\n" +
            "      \"Federal Housing Complex Rd, Victoria Island, Lagos, Nigeria\"\n" +
            "   ],\n" +
            "   \"origin_addresses\" : [\n" +
            "      \"Keystone Bank Crescent, Adeyemo Alakija St, Victoria Island, Lagos, Nigeria\"\n" +
            "   ],\n" +
            "   \"rows\" : [\n" +
            "      {\n" +
            "         \"elements\" : [\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"1.4 mi\",\n" +
            "                  \"value\" : 2190\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"7 mins\",\n" +
            "                  \"value\" : 427\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"1.4 mi\",\n" +
            "                  \"value\" : 2318\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"7 mins\",\n" +
            "                  \"value\" : 439\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.7 mi\",\n" +
            "                  \"value\" : 1197\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"6 mins\",\n" +
            "                  \"value\" : 356\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.9 mi\",\n" +
            "                  \"value\" : 1452\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"4 mins\",\n" +
            "                  \"value\" : 253\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.9 mi\",\n" +
            "                  \"value\" : 1452\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"4 mins\",\n" +
            "                  \"value\" : 253\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.3 mi\",\n" +
            "                  \"value\" : 554\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"2 mins\",\n" +
            "                  \"value\" : 132\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.3 mi\",\n" +
            "                  \"value\" : 554\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"2 mins\",\n" +
            "                  \"value\" : 132\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.3 mi\",\n" +
            "                  \"value\" : 554\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"2 mins\",\n" +
            "                  \"value\" : 132\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.3 mi\",\n" +
            "                  \"value\" : 554\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"2 mins\",\n" +
            "                  \"value\" : 132\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.6 mi\",\n" +
            "                  \"value\" : 1021\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"4 mins\",\n" +
            "                  \"value\" : 225\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.7 mi\",\n" +
            "                  \"value\" : 1121\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"4 mins\",\n" +
            "                  \"value\" : 249\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.9 mi\",\n" +
            "                  \"value\" : 1403\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"4 mins\",\n" +
            "                  \"value\" : 233\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.9 mi\",\n" +
            "                  \"value\" : 1403\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"4 mins\",\n" +
            "                  \"value\" : 233\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"0.9 mi\",\n" +
            "                  \"value\" : 1403\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"4 mins\",\n" +
            "                  \"value\" : 233\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"1.3 mi\",\n" +
            "                  \"value\" : 2114\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"7 mins\",\n" +
            "                  \"value\" : 399\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"1.3 mi\",\n" +
            "                  \"value\" : 2017\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"6 mins\",\n" +
            "                  \"value\" : 375\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"1.3 mi\",\n" +
            "                  \"value\" : 2029\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"6 mins\",\n" +
            "                  \"value\" : 383\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"1.3 mi\",\n" +
            "                  \"value\" : 2042\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"6 mins\",\n" +
            "                  \"value\" : 380\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"1.2 mi\",\n" +
            "                  \"value\" : 1871\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"8 mins\",\n" +
            "                  \"value\" : 479\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"distance\" : {\n" +
            "                  \"text\" : \"1.4 mi\",\n" +
            "                  \"value\" : 2324\n" +
            "               },\n" +
            "               \"duration\" : {\n" +
            "                  \"text\" : \"7 mins\",\n" +
            "                  \"value\" : 442\n" +
            "               },\n" +
            "               \"status\" : \"OK\"\n" +
            "            }\n" +
            "         ]\n" +
            "      }\n" +
            "   ],\n" +
            "   \"status\" : \"OK\"\n" +
            "}";
    private String json = "{\n" +
            "   \"html_attributions\" : [],\n" +
            "   \"next_page_token\" : \"CqQCHAEAAEC23JhA4KLOQTuIYDy7dHK893l7-AhOcScMVTG6bSOjro-3T2Wq6r-dFBVECUN0OpQSSQ6LAjGslee7r_Hmh2tnsJDKh-HQPwJROVVCQiBIWC2sIYYp0TyGFth8Y-t3M3U0WspxN-rUOswaQzAIYDfPV6UG4biKtpT2_giPKVVs8JH66p4QEB3O7bD3950BmkKf05wEic5fCa0JHVgUVi-ucyjTqZlUU-pDL946SC5d37blzM-WTFADz3TaxbomL3jTlBRj2DwikA12bIdPcaanZ9jYVj2sK1bzHxSXnoPu41A11_6Od6O-YFXh57Ts4MjJyN0wYdLBlR4h3jDi3fGEFU1x4j2n1mc-YW-3dEDiz5sYU2pe1l6mTMTvGVNN5BIQ7B7p-mCKyz-VYy-tdDoDIxoU6CoDEOGYgAN_iG9-EUULLGZkYQk\",\n" +
            "   \"results\" : [\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.428055,\n" +
            "               \"lng\" : 3.421944\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.429979030291502,\n" +
            "                  \"lng\" : 3.423400230291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.427281069708497,\n" +
            "                  \"lng\" : 3.420702269708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"2ac495899309e6cf4ffab773832d38e8394e73fa\",\n" +
            "         \"name\" : \"Guaranty Trust Bank PLC\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"photos\" : [\n" +
            "            {\n" +
            "               \"height\" : 437,\n" +
            "               \"html_attributions\" : [\n" +
            "                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/100756749077811020102/photos\\\"\\u003eProspa Richy\\u003c/a\\u003e\"\n" +
            "               ],\n" +
            "               \"photo_reference\" : \"CmRaAAAA6S80D9Vg2-i7KsmAhGd25sroTINE5OqyKghcaVfilYMhKnwKBy-wfPJgNQOPrTZT5wy0kHzqnUHSrrdedB3IVaMt1VGCna1ZpjejDT5VSNJvRQX8z-06HUDI3gtbcvPJEhDoPEKxIUiVHR9ctEVvsTY3GhSgvCfGTsSE6cc_XBysBv6rNWTh2w\",\n" +
            "               \"width\" : 776\n" +
            "            }\n" +
            "         ],\n" +
            "         \"place_id\" : \"ChIJRZkiE0GDshERa_WRrqSDmks\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCHC+6Q Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCHC+6Q\"\n" +
            "         },\n" +
            "         \"rating\" : 4.5,\n" +
            "         \"reference\" : \"ChIJRZkiE0GDshERa_WRrqSDmks\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"bank\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"close off adeola odeku logos., Adeola Odeku Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4305337,\n" +
            "               \"lng\" : 3.4155632\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.431882680291502,\n" +
            "                  \"lng\" : 3.416912180291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.429184719708497,\n" +
            "                  \"lng\" : 3.414214219708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"d87890b956af6c437758acda4a8fbbee55219a72\",\n" +
            "         \"name\" : \"Enterprise Bank - Adeola Odeku\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"place_id\" : \"ChIJHWtstdOKOxAR9O0h_2TiqMk\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCJ8+66 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCJ8+66\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJHWtstdOKOxAR9O0h_2TiqMk\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"40 Adeola Odeku St, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.432910399999999,\n" +
            "               \"lng\" : 3.4256909\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.434438180291502,\n" +
            "                  \"lng\" : 3.427000780291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.431740219708497,\n" +
            "                  \"lng\" : 3.424302819708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bank_dollar-71.png\",\n" +
            "         \"id\" : \"dec069fd8b649727ae4cd8c3127b8ab2dfc8fa9f\",\n" +
            "         \"name\" : \"Wema Bank - Idowu Taylor\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"photos\" : [\n" +
            "            {\n" +
            "               \"height\" : 3096,\n" +
            "               \"html_attributions\" : [\n" +
            "                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/104632654651988290518/photos\\\"\\u003eSanya Oluwadare\\u003c/a\\u003e\"\n" +
            "               ],\n" +
            "               \"photo_reference\" : \"CmRaAAAAUHtkTdfVgDG4qcZG293WF1Bxv3t4Z-oYP2JbjWYUO8-YwmBoxdLuVMdEtPYLUfgQATC5vTIUxa4z4GxgmqFpKb5C_rOoNcZ6PR1qLYHaql5jJBnixCJAf5Vr8GPYxkMxEhBdDx5kbGaecr49IIfLnB94GhQRHSxqOh4GjWFbXQc1i4hNdpzzMw\",\n" +
            "               \"width\" : 4128\n" +
            "            }\n" +
            "         ],\n" +
            "         \"place_id\" : \"ChIJDeD8zS71OxARoR3vsD3vNcI\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCMG+57 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCMG+57\"\n" +
            "         },\n" +
            "         \"rating\" : 3.8,\n" +
            "         \"reference\" : \"ChIJDeD8zS71OxARoR3vsD3vNcI\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"bank\", \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"8 Idowu Taylor Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4316555,\n" +
            "               \"lng\" : 3.4239345\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.433004480291502,\n" +
            "                  \"lng\" : 3.425283480291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.430306519708497,\n" +
            "                  \"lng\" : 3.422585519708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"eabb546fbccd4b6516b3f997290ba0dfb1647e67\",\n" +
            "         \"name\" : \"Skye Bank - Cash Deposit ATM\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"place_id\" : \"ChIJCTgAMyz1OxAR1w7h33t1_CA\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCJF+MH Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCJF+MH\"\n" +
            "         },\n" +
            "         \"rating\" : 4,\n" +
            "         \"reference\" : \"ChIJCTgAMyz1OxAR1w7h33t1_CA\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"3 Akin Adesola St, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4316555,\n" +
            "               \"lng\" : 3.4239345\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.433004480291502,\n" +
            "                  \"lng\" : 3.425283480291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.430306519708497,\n" +
            "                  \"lng\" : 3.422585519708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"05c9db69e533fe977ccf6118337dc77be7f27b33\",\n" +
            "         \"name\" : \"Skye Bank - Akin Adesola Onsite 2\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"place_id\" : \"ChIJCTgAMyz1OxARe7IOatXi3G8\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCJF+MH Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCJF+MH\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJCTgAMyz1OxARe7IOatXi3G8\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"3 Akin Adesola St, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4375266,\n" +
            "               \"lng\" : 3.4278564\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.438888980291502,\n" +
            "                  \"lng\" : 3.429499480291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.436191019708497,\n" +
            "                  \"lng\" : 3.426801519708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"898bb576c5256a2d8bcae526933273a42ba2bb17\",\n" +
            "         \"name\" : \"Skye Bank - AHSB Onsite 5\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"place_id\" : \"ChIJ49yRRyj1OxARjAK9nwQxnVc\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCQH+24 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCQH+24\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJ49yRRyj1OxARjAK9nwQxnVc\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"Plot 1261 Adeola Hopewell Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4375266,\n" +
            "               \"lng\" : 3.4278564\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.438888980291502,\n" +
            "                  \"lng\" : 3.429499480291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.436191019708497,\n" +
            "                  \"lng\" : 3.426801519708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"25997b753fe70e84905870d55d4109093cbf7b2a\",\n" +
            "         \"name\" : \"Skye Bank - Kiosk 1\",\n" +
            "         \"place_id\" : \"ChIJY9yVVeqHNxAR9hbn2PiIzLk\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCQH+24 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCQH+24\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJY9yVVeqHNxAR9hbn2PiIzLk\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"Plot 1261 Adeola Hopewell Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4375266,\n" +
            "               \"lng\" : 3.4278564\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.438888980291502,\n" +
            "                  \"lng\" : 3.429499480291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.436191019708497,\n" +
            "                  \"lng\" : 3.426801519708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"9447bfb37fff357d89f63c660fc451c3e10cd42c\",\n" +
            "         \"name\" : \"Skye Bank - AHSB Onsite 6\",\n" +
            "         \"place_id\" : \"ChIJ49yRRyj1OxARckh5nbynYbI\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCQH+24 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCQH+24\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJ49yRRyj1OxARckh5nbynYbI\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"Plot 1261 Adeola Hopewell Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4375266,\n" +
            "               \"lng\" : 3.4278564\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.438888980291502,\n" +
            "                  \"lng\" : 3.429499480291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.436191019708497,\n" +
            "                  \"lng\" : 3.426801519708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"87925cba9c2c4d4e3f5053bd94fd3357a0d59e8d\",\n" +
            "         \"name\" : \"Skye Bank - AHSB Onsite 4\",\n" +
            "         \"place_id\" : \"ChIJ49yRRyj1OxAR0faAb-_ZoNU\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCQH+24 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCQH+24\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJ49yRRyj1OxAR0faAb-_ZoNU\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"Plot 1261 Adeola Hopewell Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4346411,\n" +
            "               \"lng\" : 3.4293844\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.435990180291502,\n" +
            "                  \"lng\" : 3.430716880291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.433292219708497,\n" +
            "                  \"lng\" : 3.428018919708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"f41a32d9b129fc56ae5fbdc79da9bacf53c76eb6\",\n" +
            "         \"name\" : \"Union Bank\",\n" +
            "         \"place_id\" : \"ChIJR8tDoSj1OxARwe_k2pOIytY\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCMH+VQ Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCMH+VQ\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJR8tDoSj1OxARwe_k2pOIytY\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"33 Adeola Hopewell Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.433743,\n" +
            "               \"lng\" : 3.429309\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.435091980291502,\n" +
            "                  \"lng\" : 3.430657980291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.432394019708497,\n" +
            "                  \"lng\" : 3.427960019708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bank_dollar-71.png\",\n" +
            "         \"id\" : \"0bc44971b7cc4127c195b76e5afab2e0cd7bc27e\",\n" +
            "         \"name\" : \"Union Bank ATM\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"photos\" : [\n" +
            "            {\n" +
            "               \"height\" : 1195,\n" +
            "               \"html_attributions\" : [\n" +
            "                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/109021919043392669000/photos\\\"\\u003eAustin Chikwado Ofor\\u003c/a\\u003e\"\n" +
            "               ],\n" +
            "               \"photo_reference\" : \"CmRaAAAAbrfhykwc9e4f7DW2lLGf7LwPsJFfQGTXy95qYuIAkke_GAEbbrOj503vqDMTSS40TcgRzsqtZZ_qIAFjJKS0d4_r-SEKqy6jBVW_KJvfXp4ItpgGn5P8YydPQEx73_hyEhC7fF66mgfK13SsbMhVcux8GhROqrN0LaGgcgoHRs-uyu9UEa38Cg\",\n" +
            "               \"width\" : 764\n" +
            "            }\n" +
            "         ],\n" +
            "         \"place_id\" : \"ChIJXeB9rCj1OxARINH9-aALAE0\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCMH+FP Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCMH+FP\"\n" +
            "         },\n" +
            "         \"rating\" : 4,\n" +
            "         \"reference\" : \"ChIJXeB9rCj1OxARINH9-aALAE0\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"bank\", \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"PC33 Adeola Hopewell, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.435439,\n" +
            "               \"lng\" : 3.430625\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.436787980291502,\n" +
            "                  \"lng\" : 3.431969680291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.434090019708497,\n" +
            "                  \"lng\" : 3.429271719708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"bc3ac60399b4d6d420b9c250d2f8550ef0311b54\",\n" +
            "         \"name\" : \"Skye Bank - Adetokunbo Onsite 3\",\n" +
            "         \"place_id\" : \"ChIJP7-POSb1OxARQYRj4K0ftlY\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCPJ+56 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCPJ+56\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJP7-POSb1OxARQYRj4K0ftlY\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"Adetokunbo Ademola Street, Lekki\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.435439,\n" +
            "               \"lng\" : 3.430625\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.436787980291502,\n" +
            "                  \"lng\" : 3.431969680291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.434090019708497,\n" +
            "                  \"lng\" : 3.429271719708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"400e94f2615451c5f9c58d70ac509c4a3b629ba8\",\n" +
            "         \"name\" : \"Skye Bank - Adetokunbo Onsite 1\",\n" +
            "         \"place_id\" : \"ChIJP7-POSb1OxAR8cpo4MTrL78\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCPJ+56 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCPJ+56\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJP7-POSb1OxAR8cpo4MTrL78\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"Adetokunbo Ademola Street, Lekki\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.435439,\n" +
            "               \"lng\" : 3.430625\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.436787980291502,\n" +
            "                  \"lng\" : 3.431969680291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.434090019708497,\n" +
            "                  \"lng\" : 3.429271719708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"2c97188eae2d0617a826a970c95a47342ca77716\",\n" +
            "         \"name\" : \"Skye Bank - Adetokunbo Onsite 2\",\n" +
            "         \"place_id\" : \"ChIJP7-POSb1OxARz0S4C_Ok0qM\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCPJ+56 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCPJ+56\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJP7-POSb1OxARz0S4C_Ok0qM\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"Adetokunbo Ademola Street, Lekki\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.429042599999998,\n" +
            "               \"lng\" : 3.422139\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.430471880291502,\n" +
            "                  \"lng\" : 3.423502980291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.427773919708497,\n" +
            "                  \"lng\" : 3.420805019708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"d1d697d55f921a8e907a52a5f65bfdb1d1e1f36a\",\n" +
            "         \"name\" : \"GTExpress\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"photos\" : [\n" +
            "            {\n" +
            "               \"height\" : 2736,\n" +
            "               \"html_attributions\" : [\n" +
            "                  \"\\u003ca href=\\\"https://maps.google.com/maps/contrib/117211612650594383321/photos\\\"\\u003ebassey edoho\\u003c/a\\u003e\"\n" +
            "               ],\n" +
            "               \"photo_reference\" : \"CmRaAAAARD6W_0v1xAOYi0qQt9IfnbR4FCVFNJs_gGR9-uvAIO_IVaLGA0kLDYsbxIYYB3oM8rzQdLKeW0MAzgzCBv_lhP1yj47awzURxiv-ev4nGoyEoUuFfhUuzlJUz0XQdFjPEhDIv3kt07i7wOFt4nNgQdHZGhSyCUbXg_COYMB2vx34WjbIF90N7A\",\n" +
            "               \"width\" : 4864\n" +
            "            }\n" +
            "         ],\n" +
            "         \"place_id\" : \"ChIJq5dBnS31OxARwthI6MkBKUg\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCHC+JV Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCHC+JV\"\n" +
            "         },\n" +
            "         \"rating\" : 4,\n" +
            "         \"reference\" : \"ChIJq5dBnS31OxARwthI6MkBKUg\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"307 Adeola Odeku Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.430951100000001,\n" +
            "               \"lng\" : 3.430762600000001\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.432284830291502,\n" +
            "                  \"lng\" : 3.432136280291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.429586869708499,\n" +
            "                  \"lng\" : 3.429438319708499\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"20a399b37cce7397bde78d45c22cdc06dfa8262a\",\n" +
            "         \"name\" : \"Skye Bank\",\n" +
            "         \"place_id\" : \"ChIJ90fMnSP1OxARAu5ZDZsba50\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCJJ+98 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCJJ+98\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJ90fMnSP1OxARAu5ZDZsba50\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"218 Ajose Adeogun Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.430981099999999,\n" +
            "               \"lng\" : 3.4307899\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.432342830291503,\n" +
            "                  \"lng\" : 3.432146130291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.429644869708499,\n" +
            "                  \"lng\" : 3.429448169708499\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"3fb2b390f9c9f487e48f91fe5b44b78b4c5d17c6\",\n" +
            "         \"name\" : \"Union Bank\",\n" +
            "         \"place_id\" : \"ChIJh-fHnyX1OxARo2MVL3D39XA\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCJJ+98 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCJJ+98\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJh-fHnyX1OxARo2MVL3D39XA\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"275 Ajose Adeogun Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4307956,\n" +
            "               \"lng\" : 3.4309979\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.432193680291502,\n" +
            "                  \"lng\" : 3.432349580291501\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.429495719708497,\n" +
            "                  \"lng\" : 3.429651619708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/bank_dollar-71.png\",\n" +
            "         \"id\" : \"c0a0f523645032bc1470653fb887e7866b1dd771\",\n" +
            "         \"name\" : \"Union Bank ATM\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"place_id\" : \"ChIJbYG1nyX1OxARLomVOy7d_Ro\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCJJ+89 Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCJJ+89\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJbYG1nyX1OxARLomVOy7d_Ro\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"bank\", \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"275 Ajose Adeogun Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.429460199999999,\n" +
            "               \"lng\" : 3.429775\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.430819380291502,\n" +
            "                  \"lng\" : 3.431123930291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.428121419708497,\n" +
            "                  \"lng\" : 3.428425969708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"761daaddd97e5aaef187f7be475df526fc6a849b\",\n" +
            "         \"name\" : \"Union Bank\",\n" +
            "         \"place_id\" : \"ChIJkcixXyX1OxARrUcYJmhMRnU\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCHH+QW Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCHH+QW\"\n" +
            "         },\n" +
            "         \"reference\" : \"ChIJkcixXyX1OxARrUcYJmhMRnU\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"1668 Oyin Jolayemi Street, Lagos\"\n" +
            "      },\n" +
            "      {\n" +
            "         \"geometry\" : {\n" +
            "            \"location\" : {\n" +
            "               \"lat\" : 6.4384752,\n" +
            "               \"lng\" : 3.4322321\n" +
            "            },\n" +
            "            \"viewport\" : {\n" +
            "               \"northeast\" : {\n" +
            "                  \"lat\" : 6.439886230291503,\n" +
            "                  \"lng\" : 3.433580080291502\n" +
            "               },\n" +
            "               \"southwest\" : {\n" +
            "                  \"lat\" : 6.437188269708498,\n" +
            "                  \"lng\" : 3.430882119708498\n" +
            "               }\n" +
            "            }\n" +
            "         },\n" +
            "         \"icon\" : \"https://maps.gstatic.com/mapfiles/place_api/icons/atm-71.png\",\n" +
            "         \"id\" : \"d8b9766f95fd8031489fa3b3576c942cf3753c84\",\n" +
            "         \"name\" : \"GT Bank\",\n" +
            "         \"opening_hours\" : {\n" +
            "            \"open_now\" : true\n" +
            "         },\n" +
            "         \"place_id\" : \"ChIJn1WeeCf1OxAR6gkgmOyJNQs\",\n" +
            "         \"plus_code\" : {\n" +
            "            \"compound_code\" : \"CCQJ+9V Lagos, Nigeria\",\n" +
            "            \"global_code\" : \"6FR5CCQJ+9V\"\n" +
            "         },\n" +
            "         \"rating\" : 4,\n" +
            "         \"reference\" : \"ChIJn1WeeCf1OxAR6gkgmOyJNQs\",\n" +
            "         \"scope\" : \"GOOGLE\",\n" +
            "         \"types\" : [ \"atm\", \"finance\", \"point_of_interest\", \"establishment\" ],\n" +
            "         \"vicinity\" : \"Federal Housing Complex Road, Lagos\"\n" +
            "      }\n" +
            "   ],\n" +
            "   \"status\" : \"OK\"\n" +
            "}";

    public Remote() {
        Gson gson = new Gson();
        mPlaceApiResponse = gson.fromJson(json, PlaceApiResponse.class);
        mDistanceMatrixApiResponse = gson.fromJson(jsonDist, DistanceMatrixApiResponse.class);
        mDirectionApiResponse = gson.fromJson(jsonDir, DirectionApiResponse.class);

        mRestService = retrofit().create(RestService.class);
    }

    private Retrofit retrofit() {
        return new Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public PlaceApiResponse getPlaces() {
        return mPlaceApiResponse;
    }

    public DistanceMatrixApiResponse getDistance() {
        return mDistanceMatrixApiResponse;
    }

    public DirectionApiResponse getDirection() {
        return mDirectionApiResponse;
    }

    public void searchNearByAtms(double lat, double lng, final NearByResult nearByResult) {
        Call<PlaceApiResponse> responseCall =
                mRestService.getNearByResponse(buildNearByUrl(lat, lng));
        responseCall.enqueue(new Callback<PlaceApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<PlaceApiResponse> call,
                                   @NonNull Response<PlaceApiResponse> response) {
                PlaceApiResponse placeResponse = response.body();
                if (placeResponse != null) {
                    nearByResult.onSuccessful(placeResponse.getResults());
                    nearByResult.onFailed(placeResponse.getStatus());
                } else {
                    nearByResult.onFailed("An error was encountered while loading nearby atms: ");
                }
            }

            @Override
            public void onFailure(@NonNull Call<PlaceApiResponse> call, @NonNull Throwable t) {
                nearByResult.onFailed("An error was encountered while loading nearby atms");
                Log.e(TAG, "onFailure: An error was encountered while loading nearby atms");
            }
        });
//        nearByResult.onSuccessful(mPlaceApiResponse.getResults());
    }

    public void getPathToAtmFromService(final NearByAtm atm, double originLat,
                                        double originLng, final DirectionToNearByAtmResult result) {
        Call<DirectionApiResponse> responseCall =
                mRestService.getDirectionResponse(
                        buildDirectionPathUrl(originLat, originLng, atm.getLat(), atm.getLng()));
        responseCall.enqueue(new Callback<DirectionApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<DirectionApiResponse> call,
                                   @NonNull Response<DirectionApiResponse> response) {
                DirectionApiResponse directionResponse = response.body();
                if (directionResponse != null) {
                    if (directionResponse.getRoutes().size() > 0) {
                        atm.setPath(directionResponse.getRoutes().get(0)
                                .getOverviewPolyline().getPoints());
                        result.onSuccessful(atm);

                        result.onFailed(directionResponse.getStatus());
                    }
                } else {
                    result.onFailed("An error was encountered while" +
                            " getting the direction to this nearby atms");
                }
            }

            @Override
            public void onFailure(@NonNull Call<DirectionApiResponse> call, @NonNull Throwable t) {
                result.onFailed("An error was encountered while" +
                        " getting the direction to this nearby atms");

                Log.e(TAG, "onFailure: An error was encountered while " +
                        "getting the direction to this nearby atms");
            }
        });
//        atm.setPath(mDirectionApiResponse.getRoutes().get(0).getOverviewPolyline().getPoints());
//        result.onSuccessful(atm);
    }

    public void getDistance(final List<Result> results, double originLat,
                            double originLng, final DistanceApiResult result) {
        Call<DistanceMatrixApiResponse> responseCall =
                mRestService.getDistanceResponse(buildDistanceUrl(results, originLat, originLng));
        responseCall.enqueue(new Callback<DistanceMatrixApiResponse>() {
            @Override
            public void onResponse(@NonNull Call<DistanceMatrixApiResponse> call, @NonNull Response<DistanceMatrixApiResponse> response) {
                DistanceMatrixApiResponse distanceResponse = response.body();
                if (distanceResponse != null) {
                    for (int i = 0; i < results.size(); i++) {
                        if (distanceResponse.getRows().size() > 0) {
                            Result res = results.get(i);
                            String distance = distanceResponse.getRows().get(0).getElements()
                                    .get(i).getDistance().getText();
                            Log.e(TAG, "onResponse: " + distance);
                            String time = distanceResponse.getRows().get(0).getElements()
                                    .get(i).getDuration().getText();

                            result.onSuccessful(res.getId(), distance, time);
                        }
                    }
                    result.onFailed(distanceResponse.getStatus());
                } else {
                    result.onFailed("Could not load distance and duration to each atm");
                }
            }

            @Override
            public void onFailure(@NonNull Call<DistanceMatrixApiResponse> call, @NonNull Throwable t) {
                result.onFailed("Could not load distance and duration to each atm");
                Log.e(TAG, "onFailure: Could not load distance and duration to each atm");
            }
        });

    }

    public interface DistanceApiResult {
        void onSuccessful(String id, String dist, String time);
        void onFailed(String error);
    }

    public interface DirectionToNearByAtmResult {
        void onSuccessful(NearByAtm atm);
        void onFailed(String error);
    }

    public interface NearByResult {
        void onSuccessful(List<Result> results);
        void onFailed(String error);
    }

    private String buildNearByUrl(double lat, double lng) {

        return "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" +
                lat + "," + lng +
                "&radius=1500&type=atm&key=AIzaSyDWebHae4-bRcIw0qcRJi5yV4JonfG0SoU";
    }

    private String buildDistanceUrl(List<Result> resultList, double originLat, double originLng) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < resultList.size(); i++) {
            Result result = resultList.get(i);

            sb.append(result.getGeometry().getLocation().getLat())
                    .append(",")
                    .append(result.getGeometry().getLocation().getLng());

            if (i != resultList.size() - 1) {
                sb.append("|");
            } else {
                break;
            }
        }

        return "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins="
                + originLat + "," + originLng
                +"&destinations="
                +sb.toString()
                +"&key=AIzaSyDWebHae4-bRcIw0qcRJi5yV4JonfG0SoU";
    }

    private String buildDirectionPathUrl(double originLat, double originLng,
                                         double destLat, double destLng) {
        return "https://maps.googleapis.com/maps/api/directions/json?origin="
                + originLat + "," + originLng
                + "&destination="
                + destLat + "," + destLng
                + "&key=AIzaSyDWebHae4-bRcIw0qcRJi5yV4JonfG0SoU";
    }
}

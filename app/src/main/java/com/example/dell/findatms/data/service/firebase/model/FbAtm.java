package com.example.dell.findatms.data.service.firebase.model;

import com.example.dell.findatms.data.db.Review;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FbAtm {

    public Map<String, Review> reviews = new HashMap<>();

    public FbAtm() {
    }

    public FbAtm(Map<String, Review> reviews) {
        this.reviews = reviews;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("reviews", reviews);

        return result;
    }
}

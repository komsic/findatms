package com.example.dell.findatms.data.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import com.example.dell.findatms.data.db.utils.ReviewConverter;

import java.util.ArrayList;
import java.util.List;

@Dao
@TypeConverters(ReviewConverter.class)
public interface NearByAtmDao {

    @Query("SELECT * FROM NearByAtm")
    LiveData<List<NearByAtm>> getAllNearByAtms();

    @Query("SELECT * FROM NearByAtm WHERE id = :id")
    LiveData<NearByAtm> getNearByAtmById(String id);

    @Query("SELECT path FROM NearByAtm WHERE id = :id")
    LiveData<String> getPathToAtm(String id);

    @Query("UPDATE NearByAtm "
            + "SET path = :path "
            + "WHERE id = :id")
    int setPathToAtm(String id, String path);

    @Query("UPDATE NearByAtm "
            + "SET reviews = :reviews "
            + "WHERE id = :id")
    int updateReviewList(ArrayList<Review> reviews, String id);

    @Query("UPDATE NearByAtm "
            + "SET distance_to_current_place = :dist, "
            + "driving_time = :time "
            + "WHERE id = :id")
    int updateDistanceAndTime(String id, String dist, String time);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertNearByAtm(NearByAtm atm);

    @Query("DELETE FROM NearByAtm")
    int deleteAll();
}
